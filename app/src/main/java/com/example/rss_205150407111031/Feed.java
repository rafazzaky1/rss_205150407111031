package com.example.rss_205150407111031;

public class Feed {
    public String url, title, link, author, description, image;

    public Feed (String url, String title, String link, String author, String description, String image){
        this.url = url;
        this.title = title;
        this.link = link;
        this.author = author;
        this.description = description;
        this.image = image;
    }
}
